package com.example.android.a4webtestapp;

import android.app.Activity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import static android.content.ContentValues.TAG;

/**
 * Created by kristian on 08/01/2018.
 */

class MyGestureListener {

    // variables for gesture detecting
    private float mPrimStartTouchEventX = -1;
    private float mPrimStartTouchEventY = -1;
    private float mSecStartTouchEventX = -1;
    private float mSecStartTouchEventY = -1;
    private float mThrStartTouchEventX = -1;
    private float mThrStartTouchEventY = -1;

    private boolean mPrimSwipedLeft = false;
    private boolean mSecSwipedLeft = false;
    private boolean mPrimSwipedDown = false;
    private boolean mSecSwipedDown = false;
    private boolean mThrSwipedDown = false;
    private boolean disableSwipeLeft = false;
    private boolean disableSwipeDown = false;

    final  int SWIPE_THRESHOLD = 25;
    private int mPtrCount=0;

    private final String TAG = this.getClass().getSimpleName();

    MyGestures myGestures;

    public MyGestureListener(MyGestures myGestures){
        this.myGestures = myGestures;
    }


    public void onTouchEvent(MotionEvent event){

        int action = (event.getAction() & MotionEvent.ACTION_MASK);

        switch (action) {
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_DOWN:
                mPtrCount++;
                if (mPtrCount == 1) {
                    mPrimStartTouchEventX = event.getX(0);
                    mPrimStartTouchEventY = event.getY(0);
                    mPrimSwipedLeft = false;
                    mPrimSwipedDown = false;
                    //Log.d(TAG, String.format("POINTER ONE X = %.5f, Y = %.5f", mPrimStartTouchEventX, mPrimStartTouchEventY));
                }
                if (mPtrCount == 2) {
                    mSecStartTouchEventX = event.getX(1);
                    mSecStartTouchEventY = event.getY(1);
                    mSecSwipedLeft = false;
                    mSecSwipedDown = false;
                    //Log.d(TAG, String.format("POINTER TWO X = %.5f, Y = %.5f", mSecStartTouchEventX, mSecStartTouchEventY));
                }
                if (mPtrCount == 3) {
                    mThrStartTouchEventX = event.getX(2);
                    mThrStartTouchEventY = event.getY(2);
                    mThrSwipedDown = false;
                    //Log.d(TAG, String.format("POINTER TWO X = %.5f, Y = %.5f", mThrStartTouchEventX, mThrStartTouchEventY));
                }
                break;
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_UP:

                if (mPtrCount > 2){
                    disableSwipeLeft = true;
                }

                if (mPtrCount > 3){
                    disableSwipeDown = true;
                }

                if (mPtrCount == 3){
                    mThrSwipedDown = didPointerSwipedDown(mThrStartTouchEventX, mThrStartTouchEventY, event.getX(2), event.getY(2));
                }

                if (mPtrCount == 2){
                    mSecSwipedDown = didPointerSwipedDown(mSecStartTouchEventX, mSecStartTouchEventY, event.getX(1), event.getY(1));
                    mSecSwipedLeft = didPointerSwipedLeft(mSecStartTouchEventX, mSecStartTouchEventY, event.getX(1), event.getY(1));
                }
                if (mPtrCount == 1){
                    if (!disableSwipeLeft) mPrimSwipedLeft = didPointerSwipedLeft(mPrimStartTouchEventX, mPrimStartTouchEventY, event.getX(0), event.getY(0));
                    if (!disableSwipeDown) mPrimSwipedDown = didPointerSwipedDown(mPrimStartTouchEventX, mPrimStartTouchEventY, event.getX(0), event.getY(0));
                }

                if (mPrimSwipedLeft && mSecSwipedLeft){
                    myGestures.onTwoBackSwipe();
                    //Log.d(TAG, "on back swipe");
                }

                if (mPrimSwipedDown && mSecSwipedDown && mThrSwipedDown){
                    myGestures.onThreeDownSwipe();
                    //Log.d(TAG, "on three down swipe");
                    //finish();
                    //System.exit(0);
                }

                mPtrCount--;
                if (mPtrCount < 3) {
                    mThrStartTouchEventX = -1;
                    mThrStartTouchEventY = -1;
                }
                if (mPtrCount < 2) {
                    mSecStartTouchEventX = -1;
                    mSecStartTouchEventY = -1;
                }
                if (mPtrCount < 1) {
                    mPrimStartTouchEventX = -1;
                    mPrimStartTouchEventY = -1;
                    disableSwipeLeft = false;
                    disableSwipeDown = false;
                }
                break;

            case MotionEvent.ACTION_MOVE:
                Log.d(TAG, "fingers: " + mPtrCount);
                //Log.d(TAG, "Move " + mPtrCount);
                break;
        }

    }

    public boolean didPointerSwipedLeft (float startX, float startY, float endX, float endY){

        float diffX = endX - startX;
        float diffY = endY - startY;

        if (Math.abs(diffX)>Math.abs(diffY) && Math.abs(diffX) > SWIPE_THRESHOLD && diffX<0){
            return true;
        }
        return false;
    }

    public boolean didPointerSwipedDown (float startX, float startY, float endX, float endY){

        float diffX = endX - startX;
        float diffY = endY - startY;

        if (Math.abs(diffX)<Math.abs(diffY) && Math.abs(diffY) > SWIPE_THRESHOLD && diffY>0){
            return true;
        }
        return false;
    }

    public interface MyGestures{
        void onTwoBackSwipe();
        void onThreeDownSwipe();
    }


}







