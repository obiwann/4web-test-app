package com.example.android.a4webtestapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by kristian on 10/01/2018.
 */

public class MyCanvasView extends View implements MyGestureListener.MyGestures{

    public int width;
    public int height;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    Context context;
    private Paint mPaint;
    private float mX, mY;
    private static final float TOLERANCE = 25;
    private final String TAG = this.getClass().getSimpleName();
    int viewWidth;
    int viewHeight;
    int viewCenterWidth;
    int viewCenterHeight;
    int shape;

    float squareLeft;
    float squareTop;
    float squreRight;
    float sQuareBottom;

    private int mPtrCount=0;
    private MyGestureListener.MyGestures myGesturesCallback;
    private MyGestureListener myGestureListener;

    public MyCanvasView(Context c, AttributeSet attrs) {
        super(c, attrs);
        context = c;

        // we set a new Path
        mPath = new Path();

        // and we set a new Paint with the desired attributes
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(4f);
        myGestureListener = new MyGestureListener(this);


    }

    public void setMyGesturesCallback(MyGestureListener.MyGestures myGestures){
        this.myGesturesCallback = myGestures;
    }

    public void drawCircle(float heightPercentege){
        clearCanvas();
        // max size of radius is the same as viewcenterheight
        float radius = viewCenterHeight * (heightPercentege/100);
        shape = Screen2Activity.SHAPE_CIRCLE;
        Log.d(TAG, "radius is" + radius);
        Log.d(TAG, "height: " + viewCenterHeight + " width: " + viewCenterWidth);
        mPath.addCircle(viewCenterWidth, viewCenterHeight, radius, Path.Direction.CW);
        invalidate();

    }

    public void drawSquare (float heightPercentage){
        clearCanvas();
        float height = viewHeight * (heightPercentage/100);
        float squareLineDistanceFromCenter = height/2;
        shape = Screen2Activity.SHAPE_SQUARE;
        squareLeft = viewCenterWidth-squareLineDistanceFromCenter;
        squareTop = viewCenterHeight-squareLineDistanceFromCenter;
        squreRight = viewCenterWidth+squareLineDistanceFromCenter;
        sQuareBottom = viewCenterHeight+squareLineDistanceFromCenter;

        mPath.addRect(squareLeft, squareTop, squreRight, sQuareBottom, Path.Direction.CW);
        invalidate();
    }

    public void drawTriangle(float heightPercentage) {

        clearCanvas();
        shape = Screen2Activity.SHAPE_TRIANGLE;

        //visina = maks visina * height percentege
        float height = viewHeight* (heightPercentage/100);
        // polmer ocrtanega kroga: R = 2* visina /3
        float radius = 2 * height/3;
        // y koordinati t1 in t3: viviewCenterHeight-(visina/2)
        float y1 = viewCenterHeight + height/2;
        float y3 = viewCenterHeight + height/2;
        //izhodisci centra ocrtanega kroga
        float p = viewCenterWidth;
        float q = viewCenterHeight + height/2 - height / 3 ;
        // enacba kroznice: (x-p)^{2}+(y-q)^{2}=r^{2}
        float x1 = (float) (p - Math.sqrt(-Math.pow(q, 2) + 2 * q * y1 + Math.pow(radius, 2) - Math.pow(y1, 2)));
        float x3 = (float) (p + Math.sqrt(-Math.pow(q, 2) + 2 * q * y3 + Math.pow(radius, 2) - Math.pow(y3, 2)));
        float x2 = viewCenterWidth;
        float y2 = viewCenterHeight - height/2;
        //ker ekran meri y od zgoraj navzdol je narobe obrnjen
        mPath.moveTo(x1,y1);
        mPath.lineTo(x2, y2);
        mPath.lineTo(x3, y3);
        mPath.lineTo(x1,y1);
        invalidate();
    }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // your Canvas will draw onto the defined Bitmap
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        viewWidth = this.getWidth();
        viewHeight = this.getHeight();
        viewCenterWidth = viewWidth/2;
        viewCenterHeight = viewHeight/2;
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the mPath with the mPaint on the canvas when onDraw
        canvas.drawPath(mPath, mPaint);
    }

    public void clearCanvas() {
        mPath.reset();
        invalidate();
    }

    public void vibrate (){
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) context.getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            ((Vibrator) context.getSystemService(VIBRATOR_SERVICE)).vibrate(100);
        }
    }


    public boolean isOnShape(float x1, float y1, float x2, float y2){
        float distance = (float) Math.hypot(x1-x2, y1-y2);
        if (distance <= TOLERANCE ){
            return true;
        }
        return false;
    }

    //override the onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        myGestureListener.onTouchEvent(event);

        int action = (event.getAction() & MotionEvent.ACTION_MASK);

        float x = event.getX();
        float y = event.getY();

        switch (action) {


            case MotionEvent.ACTION_POINTER_DOWN:
                mPtrCount++;
                //Log.d(TAG, "fingers pointer down ++ ");
                break;

            case MotionEvent.ACTION_DOWN:
                mPtrCount++;
                Log.d(TAG, "fingers action down ++ ");
                break;

            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_UP:
                //Log.d(TAG, "fingers -- ");
                mPtrCount--;
                break;

            case MotionEvent.ACTION_MOVE:
                //Log.d(TAG, "fingers: " + mPtrCount);
                if (mPtrCount==1) {
                    PathMeasure pm = new PathMeasure(mPath, false);
                    //coordinates will be here
                    float aCoordinates[] = {0f, 0f};
                    float length = pm.getLength();
                    //Log.d(TAG, "shape length: " + length);
                    //get coordinates of the middle point
                    for (float distance = 0.0f; distance < length; distance += 7.5f) {
                        pm.getPosTan(distance, aCoordinates, null);
                        if (isOnShape(x, y, aCoordinates[0], aCoordinates[1])) {
                            vibrate();
                        }
                    }
                }
                break;
        }

        return true;
        //return super.onTouchEvent(event);
    }

    @Override
    public void onTwoBackSwipe() {
        //Log.d(TAG, "onTwoBackSwipe");
        myGesturesCallback.onTwoBackSwipe();
    }

    @Override
    public void onThreeDownSwipe() {
        //Log.d(TAG, "onThreeDownSwipe");
        myGesturesCallback.onThreeDownSwipe();
    }
}