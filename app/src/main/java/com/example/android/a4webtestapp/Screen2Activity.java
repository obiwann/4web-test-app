package com.example.android.a4webtestapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.IntDef;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.Toast;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Screen2Activity extends AppCompatActivity implements MyGestureListener.MyGestures {

    final String TAG = this.getClass().getSimpleName();
    MyCanvasView myCanvasView;
    VerticalSeekBar verticalSeekBar;

    public static final int SHAPE_SQUARE = 1;
    public static final int SHAPE_TRIANGLE = 2;
    public static final int SHAPE_CIRCLE = 3;

    public static final int OVERLAY_PERMISSION_REQ_CODE = 4545;
    protected CustomViewGroup blockingView = null;

    private int selectedShape;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SHAPE_SQUARE, SHAPE_TRIANGLE, SHAPE_CIRCLE})
    public @interface Shape{}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);
        //always landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "Please give my app this permission!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            } else {
                disableStatusBar();
            }
        }
        else {
            disableStatusBar();
        }

        myCanvasView = (MyCanvasView) findViewById(R.id.my_canvas);
        myCanvasView.setMyGesturesCallback(this);
        verticalSeekBar = (VerticalSeekBar) findViewById(R.id.vertical_seekbar);

        verticalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d(TAG,"onProgressChanged: " + i);

                switch (selectedShape){
                    case SHAPE_SQUARE:
                        myCanvasView.drawSquare(i);
                        break;
                    case SHAPE_TRIANGLE:
                        myCanvasView.drawTriangle(i);
                        break;
                    case SHAPE_CIRCLE:
                        myCanvasView.drawCircle(i);
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "User can access system settings without this permission!", Toast.LENGTH_SHORT).show();
            }
            else
            { disableStatusBar();
            }
        }
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event){
//        //myCanvasView.onTouchEvent(event);
//        //return true;
//        return super.onTouchEvent(event);
//    }

    public void squareOnClick (View view){
        int progress = verticalSeekBar.getProgress();
        selectedShape = SHAPE_SQUARE;
        myCanvasView.drawSquare(progress);
    }

    public void triangleOnClick (View view){
        int progress = verticalSeekBar.getProgress();
        selectedShape = SHAPE_TRIANGLE;
        myCanvasView.drawTriangle(progress);
    }

    public void circleOnClick (View view){
        int progress = verticalSeekBar.getProgress();
        selectedShape = SHAPE_CIRCLE;
        myCanvasView.drawCircle(progress);
    }

    @Override
    public void onTwoBackSwipe() {
        Log.d(TAG, "onTwoBackSwipe");
        super.onBackPressed();
    }

    @Override
    public void onThreeDownSwipe() {
        Log.d(TAG, "onThreeDownSwipe");
        finish();
        System.exit(0);
    }

    @Override
    public void onBackPressed() {
        //do nothing
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        if (blockingView!=null) {
            WindowManager manager = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
            manager.removeView(blockingView);
        }
    }

    protected void disableStatusBar() {

        WindowManager manager = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to receive touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (25 * getResources().getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        blockingView = new CustomViewGroup(this);
        manager.addView(blockingView, localLayoutParams);
    }


}
